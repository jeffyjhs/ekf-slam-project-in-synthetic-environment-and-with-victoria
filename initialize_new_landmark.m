function initialize_new_landmark(z, R)

global Param;
global State;
x = State.Ekf.mu(1)+z(1)*cos(z(2)+State.Ekf.mu(3));
y = State.Ekf.mu(2)+z(1)*sin(z(2)+State.Ekf.mu(3));
if isempty(State.Ekf.iM)
    n = 3;
else
    n = State.Ekf.iM(length(State.Ekf.iM));
end
State.Ekf.iM    = [State.Ekf.iM, n+1, n+2];         % 2*nL vector containing map indices
if isempty(State.Ekf.iL)
    State.Ekf.iL    = {[n+1,n+2]};
else
    State.Ekf.iL{length(State.Ekf.iL)+1} = [n+1,n+2];         % nL cell array containing indices of landmark i
end
State.Ekf.sL    = [State.Ekf.sL,z(3)];         % nL vector containing signatures of landmarks
State.Ekf.nL    = State.Ekf.nL+1;          % scalar number of landmarks
State.Ekf.mu    = [State.Ekf.mu;x;y]; % robot initial pose
State.Ekf.Sigma = [State.Ekf.Sigma,zeros(size(State.Ekf.Sigma,1),2);...
                   zeros(2,size(State.Ekf.Sigma,2)),[10^6, 0 ;0 10^6]]; % robot initial covariance
Param.intcov{z(3)} = State.Ekf.Sigma(1:2,1:2);
end
