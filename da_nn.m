function Li=da_nn(z,R)
% perform nearest-neighbor data association

global Param;
global State;
Li = zeros(size(z,2),1);
newLandMark = 0;
for j = 1:size(z,2)
    minIndex = 0;
    minDis = -1;
    for i = 1:State.Ekf.nL
        xi = State.Ekf.iL{i}(1);
        yi = State.Ekf.iL{i}(2);
        dx = State.Ekf.mu(xi) - State.Ekf.mu(1);
        dy = State.Ekf.mu(yi) - State.Ekf.mu(2);
        zHat = [sqrt(dx^2+dy^2),(atan2(dy,dx)-State.Ekf.mu(3))];
        zHat(2) = minimizedAngle(zHat(2));
        H = [(-1*dx/zHat(1)),(-1*dy/zHat(1)),0,(-1*dx/zHat(1)),(-1*dy/zHat(1));...
             ((dy*dx^-2)/(1+(dy/dx)^2)),((-1*dx^-1)/(1+(dy/dx)^2)),-1,((dy*dx^-2)/(1+(dy/dx)^2)),((1*dx^-1)/(1+(dy/dx)^2))];
        P = H*[State.Ekf.Sigma(1:3,1:3),State.Ekf.Sigma(1:3,State.Ekf.iL{i}(1):State.Ekf.iL{i}(2));State.Ekf.Sigma(State.Ekf.iL{i}(1):State.Ekf.iL{i}(2),1:3),State.Ekf.Sigma(State.Ekf.iL{i}(1):State.Ekf.iL{i}(2),State.Ekf.iL{i}(1):State.Ekf.iL{i}(2))]*H'+R;
        D = (z(:,j)'-zHat)*(P\((z(:,j)'-zHat)'));
        if minDis == -1
            minDis = D;
            minIndex = State.Ekf.sL(i);
        elseif minDis > D
            minDis = D;
            minIndex = State.Ekf.sL(i);
        end
    end
    if minDis <9.7 && minDis > 0
        Li(j) = minIndex;
    else
        newLandMark = newLandMark + 1;
        Li(j) = State.Ekf.nL + newLandMark;
    end
end
end