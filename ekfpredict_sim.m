function ekfpredict_sim(u)
% EKF-SLAM prediction for simulator process model

global Param;
global State;

theta = State.Ekf.mu(3);
State.Ekf.t = State.Ekf.t + Param.deltaT;
State.Ekf.mu = prediction(State.Ekf.mu,u);

G = [1, 0, -1*u(2)*sin(theta+u(1)),zeros(1,2*State.Ekf.nL);...
     0, 1, u(2)*cos(theta+u(1)),   zeros(1,2*State.Ekf.nL);...
     0, 0, 1,                      zeros(1,2*State.Ekf.nL);...
     zeros(2*State.Ekf.nL,3),      eye(2*State.Ekf.nL)];
V = [-1*u(2)*sin(theta+u(1)), cos(theta+u(1)), 0;...
     u(2)*cos(theta+u(1)), sin(theta+u(1)), 0 ;...
     1, 0, 1;...
     zeros(2*State.Ekf.nL,3)];
M = diag([u(1)^2*Param.alphas(1)+u(2)^2*Param.alphas(2),u(2)^2*Param.alphas(3)+u(1)^2*Param.alphas(4)+u(3)^2*Param.alphas(4),u(3)^2*Param.alphas(1)+u(2)^2*Param.alphas(2)]);
State.Ekf.Sigma = G*State.Ekf.Sigma*G'+V*M*V';
end