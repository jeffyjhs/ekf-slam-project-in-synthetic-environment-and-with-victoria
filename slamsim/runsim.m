function varargout = runsim(stepsOrData, pauseLen)

global Param;
global Data;
global State;

if ~exist('pauseLen','var')
    pauseLen = 0.3; % seconds
end

% Initalize Params
%===================================================
Param.initialStateMean = [180 50 0]';

% max number of landmark observations per timestep
Param.maxObs = 2;

% number of landmarks per sideline of field (minimum is 3)
Param.nLandmarksPerSide =4;

% Motion noise (in odometry space, see p.134 in book).
Param.alphas = [0.05 0.001 0.05 0.01].^2; % std of noise proportional to alphas

% Standard deviation of Gaussian sensor noise (independent of distance)
Param.beta = [10, deg2rad(10)]; % [cm, rad]
Param.R = diag(Param.beta.^2);

% Step size between filter updates, can be less than 1.
Param.deltaT=0.1; % [s]

if isscalar(stepsOrData)
    % Generate a data set of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    Data = generateScript(Param.initialStateMean, numSteps, Param.maxObs, Param.alphas, Param.beta, Param.deltaT);
else
    % use a user supplied data set from a previous run
    Data = stepsOrData;
    numSteps = size(Data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end
%===================================================

% Initialize State
%===================================================

State.Ekf.mu = Param.initialStateMean;
State.Ekf.Sigma = zeros(3);

if Param.video
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end
if strcmp(Param.dataAssociation,'nn')
    Param.DATable = zeros(2*Param.nLandmarksPerSide,2*Param.nLandmarksPerSide);
end
Param.intcov = cell(2*Param.nLandmarksPerSide,1);
for t = 1:numSteps
    plotsim(t);

    %=================================================
    % data available to your filter at this time step
    %=================================================
    u = getControl(t);
    z = getObservations(t);

    %=================================================
    %TODO: update your filter here based upon the
    %      motionCommand and observation
    %=================================================

    ekfpredict_sim(u);
    ekfupdate(z);
    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
    
    plotmarker(State.Ekf.mu(1:2),'blue');
    plotcov2d(State.Ekf.mu(1),State.Ekf.mu(2),State.Ekf.Sigma(1:2,1:2),'blue',false,'gray',0,3);
    if State.Ekf.nL > 0
        for i = 1:State.Ekf.nL
            plotmarker(State.Ekf.mu(State.Ekf.iL{i}(1):State.Ekf.iL{i}(2)),'red');
            plotcov2d(State.Ekf.mu(State.Ekf.iL{i}(1)),State.Ekf.mu(State.Ekf.iL{i}(2)),State.Ekf.Sigma(State.Ekf.iL{i}(1):State.Ekf.iL{i}(2),State.Ekf.iL{i}(1):State.Ekf.iL{i}(2)),'red',false,'gray',0,3);
        end
    end
    drawnow;
    if Param.video
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
    if pauseLen > 0
        pause(pauseLen);
    end
end

for i = 1:State.Ekf.nL
disp(i);
disp('th landmark');
disp(Param.intcov{State.Ekf.sL(i)});
disp(State.Ekf.Sigma(State.Ekf.iL{i},State.Ekf.iL{i}));
end

% 
% f = figure();
% rnames = {'Ground Truth:0'};
% for i = 1:2*Param.nLandmarksPerSide
%     rnames{i} = strcat('Ground Truth:',num2str(i));
% end
% cnames = {'Generated:0'};
% for i = 1:size(Param.DATable,2)
%     cnames{i} = strcat('Generate:',num2str(i));
% end
% t = uitable(f,'Data',Param.DATable,...
%             'ColumnName',cnames,... 
%             'RowName',rnames);
% t.Position(3) = t.Extent(3);
% t.Position(4) = t.Extent(4);
% figure();
% subplot(2,1,1);
% i = 0:8;
% k =[1,4,6,8,10,12,14,16,18];
% [stdDev,cor]=cov2corr(State.Ekf.Sigma);
% plot(i,cor(1,k),i,cor(4,k),i,cor(6,k),i,cor(8,k),i,cor(10,k),i,cor(12,k),i,cor(14,k),i,cor(16,k),i,cor(18,k));
% hold on;
% axis([0,8,-1,1]);
% legend('0(Robot)','1','2','3','4','5','6','7','8');
% xlabel('Feature ID');
% ylabel(' \rho ');
% title('Correlation Coefficient between landmarks and robots');
% hold off;
% subplot(2,1,2);
% hold on;
% imagesc(flipud(abs(cor)));
% colormap(flipud(gray));
% axis equal;
% title('\Sigma Matrix');
% hold off;
% figure();
% hold on;
% determinant  = [det(State.Ekf.Sigma(1:3,1:3)),det(State.Ekf.Sigma(4:5,4:5)),det(State.Ekf.Sigma(6:7,6:7)),det(State.Ekf.Sigma(8:9,8:9)),det(State.Ekf.Sigma(10:11,10:11)),det(State.Ekf.Sigma(12:13,12:13)),det(State.Ekf.Sigma(14:15,14:15)),det(State.Ekf.Sigma(16:17,16:17)),det(State.Ekf.Sigma(18:19,18:19))];
% determinant = power(determinant,0.25);
% plot(i,determinant,'--b*');
% xlabel('Feature ID');
% ylabel('$$det(\Sigma_{ii})^{1/4}$$','Interpreter','LaTex');
% title('Determinant of the feature covariance matrix');
% hold off;
if Param.video
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end
if nargout >= 1
    varargout{1} = Data;
end

%==========================================================================
function u = getControl(t)
global Data;
% noisefree control command
u = Data.noisefreeControl(:,t);  % 3x1 [drot1; dtrans; drot2]


%==========================================================================
function z = getObservations(t)
global Data;
% noisy observations
z = Data.realObservation(:,:,t); % 3xn [range; bearing; landmark id]
ii = find(~isnan(z(1,:)));
z = z(:,ii);

%==========================================================================
function plotsim(t)
global Data;

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

%=================================================
% data *not* available to your filter, i.e., known
% only by the simulator, useful for making error plots
%=================================================
% actual position (i.e., ground truth)
x = Data.Sim.realRobot(1,t);
y = Data.Sim.realRobot(2,t);
theta = Data.Sim.realRobot(3,t);

% real observation
observation = Data.realObservation(:,:,t);

% noisefree observation
noisefreeObservation = Data.Sim.noisefreeObservation(:,:,t);

%=================================================
% graphics
%=================================================
figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(3,:));

% draw actual path (i.e., ground truth)
plot(Data.Sim.realRobot(1,1:t), Data.Sim.realRobot(2,1:t), 'Color', ACTUAL_PATH_COL);
plotrobot( x, y, theta, 'black', 1, ACTUAL_PATH_COL);

% draw noise free motion command path
plot(Data.Sim.noisefreeRobot(1,1:t), Data.Sim.noisefreeRobot(2,1:t), 'Color', NOISEFREE_PATH_COL);
plot(Data.Sim.noisefreeRobot(1,t), Data.Sim.noisefreeRobot(2,t), '*', 'Color', NOISEFREE_PATH_COL);

for k=1:size(observation,2)
    rng = Data.Sim.noisefreeObservation(1,k,t);
    ang = Data.Sim.noisefreeObservation(2,k,t);
    noisy_rng = observation(1,k);
    noisy_ang = observation(2,k);

    % indicate observed range and angle relative to actual position
    plot([x x+cos(theta+noisy_ang)*noisy_rng], [y y+sin(theta+noisy_ang)*noisy_rng], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free range and angle relative to actual position
    plot([x x+cos(theta+ang)*rng], [y y+sin(theta+ang)*rng], 'Color', NOISEFREE_BEARING_COLOR);
end
