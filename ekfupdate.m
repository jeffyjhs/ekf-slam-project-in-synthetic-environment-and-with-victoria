function ekfupdate(z)
% EKF-SLAM update step for both simulator and Victoria Park data set

global Param;
global State;


% returns state vector indices pairing observations with landmarks
switch lower(Param.dataAssociation)
    case 'known'
        Li = da_known(z);
        if strcmp(Param.updateMethod,'batch')
            Q = zeros(2*length(Li));
            zhat = zeros(2,length(Li));
            for i = 0:length(Li)-1
                Q((2*i)+1:(2*i)+2,(2*i)+1:(2*i)+2) = Param.R;
                if ~ismember(Li(i+1),State.Ekf.sL)
                    initialize_new_landmark(z(:,i+1),Q);
                end
            end
            H = zeros(2*length(Li),(3+2*State.Ekf.nL));
            for i = 0:length(Li)-1
                [dontcare, j] = ismember(Li(i+1),State.Ekf.sL);
                dx = State.Ekf.mu(State.Ekf.iL{j}(1)) - State.Ekf.mu(State.Ekf.iR(1));
                dy = State.Ekf.mu(State.Ekf.iL{j}(2)) - State.Ekf.mu(State.Ekf.iR(2));
                q = [dx, dy]*[dx;dy];
                zhat(:,i+1) = [sqrt(q);atan2(dy,dx)-State.Ekf.mu(3)];
                zhat(2,i+1) = minimizedAngle(zhat(2,i+1));
                F = zeros(5,3+2*State.Ekf.nL);
                F(1:3,1:3) = eye(3);
                F(4:5,(3+2*j-1):(3+2*j)) = eye(2);
                H((2*i+1):(2*i+2),:) =(1/q)*[-1*sqrt(q)*dx, -1*sqrt(q)*dy, 0, sqrt(q)*dx, sqrt(q)*dy;...
                                             dy,            -1*dx,         -1*q,-1*dy,    dx] *F;
            end
            K = State.Ekf.Sigma*H'/(H*State.Ekf.Sigma*H'+Q);
            deltaZ = zeros(2*length(Li),1);
            for i = 1:length(Li)
                deltaZ(2*i-1) = z(1,i)-zhat(1,i);
                deltaZ(2*i) = z(2,i)-zhat(2,i);
            end
            State.Ekf.mu = State.Ekf.mu + K*(deltaZ);
            State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));
            State.Ekf.Sigma = (eye(3+2*State.Ekf.nL)-K*H)*State.Ekf.Sigma;
        elseif strcmp(Param.updateMethod,'seq')
            Q = Param.R;
            for i = 1:length(Li)
                if ~ismember(Li(i),State.Ekf.sL)
                    initialize_new_landmark(z(:,i),Q);
                end
                [dontcare, j] = ismember(Li(i),State.Ekf.sL);
                dx = State.Ekf.mu(State.Ekf.iL{j}(1)) - State.Ekf.mu(State.Ekf.iR(1));
                dy = State.Ekf.mu(State.Ekf.iL{j}(2)) - State.Ekf.mu(State.Ekf.iR(2));
                q = [dx, dy]*[dx;dy];
                zhat = [sqrt(q);atan2(dy,dx)-State.Ekf.mu(3)];
                zhat(2) = minimizedAngle(zhat(2));
                F = zeros(5,3+2*State.Ekf.nL);
                F(1:3,1:3) = eye(3);
                F(4:5,(3+2*j-1):(3+2*j)) = eye(2);
                H =(1/q)*[-1*sqrt(q)*dx, -1*sqrt(q)*dy, 0, sqrt(q)*dx, sqrt(q)*dy;...
                          dy,            -1*dx,         -1*q,-1*dy,    dx] *F;
                K = State.Ekf.Sigma*H'/(H*State.Ekf.Sigma*H'+Q);
                State.Ekf.mu = State.Ekf.mu + K*(z(1:2,i)-zhat);
                State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));
                State.Ekf.Sigma = (eye(3+2*State.Ekf.nL)-K*H)*State.Ekf.Sigma;
            end
        end
    case 'nn'
        if strcmp(Param.choice,'vp')
            z(2,:) = z(2,:) - (pi/2);
        end
        Li = da_nn(z(1:2,:), Param.R);
        if strcmp(Param.updateMethod,'seq')
            Q = Param.R;
            for i = 1:length(Li)
                
                if ~ismember(Li(i),State.Ekf.sL)
%                     disp([z(1:2,i);Li(i)]);
                    initialize_new_landmark([z(1:2,i);Li(i)],Q);
                    if(State.Ekf.nL > 2*Param.nLandmarksPerSide)
                        Param.DATable = [Param.DATable,zeros(2*Param.nLandmarksPerSide,1)];
                    end
                end
                [dontcare, j] = ismember(Li(i),State.Ekf.sL);
%                 disp(State.Ekf.sL);
%                 disp(j)
                dx = State.Ekf.mu(State.Ekf.iL{j}(1)) - State.Ekf.mu(State.Ekf.iR(1));
                dy = State.Ekf.mu(State.Ekf.iL{j}(2)) - State.Ekf.mu(State.Ekf.iR(2));
                q = [dx, dy]*[dx;dy];
                zhat = [sqrt(q);atan2(dy,dx)-State.Ekf.mu(3)];
                zhat(2) = minimizedAngle(zhat(2));
                F = zeros(5,3+2*State.Ekf.nL);
                F(1:3,1:3) = eye(3);
                F(4:5,(3+2*j-1):(3+2*j)) = eye(2);
                H =(1/q)*[-1*sqrt(q)*dx, -1*sqrt(q)*dy, 0, sqrt(q)*dx, sqrt(q)*dy;...
                          dy,            -1*dx,         -1*q,-1*dy,    dx] *F;
                K = State.Ekf.Sigma*H'/(H*State.Ekf.Sigma*H'+Q);
                State.Ekf.mu = State.Ekf.mu + K*(z(1:2,i)-zhat);
                State.Ekf.mu(3) = minimizedAngle(State.Ekf.mu(3));
                State.Ekf.Sigma = (eye(3+2*State.Ekf.nL)-K*H)*State.Ekf.Sigma;
                if Param.nLandmarksPerSide~=10^10
                    Param.DATable(z(3,i),Li(i)) = Param.DATable(z(3,i),Li(i))+1;
                end
            end
        end
    case 'jcbb'
        Li = da_jcbb(z(1:2,:), Param.R);
        if strcmp(Param.updateMethod,'batch')
            
        elseif strcmp(Param.updateMethod,'seq')
            
        end
    otherwise
        error('unrecognized data association method: "%s"', Param.dataAssociation);
end
